# 网址缩短-短网址

#### 介绍
使用python3开发网址缩短业务-短网址工具网址

#### 软件架构
web框架使用python的flask 使用jin ja模板


#### 安装教程

1. 安装 python3
2. 安装 flask


#### 使用说明

1. 安装mysql数据库执行python.sql
2. 运行python flask web

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


